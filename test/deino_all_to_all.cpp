// Copyright 2023-2024 Alfredo A. Correa

// based on http://mpi.deino.net/mpi_functions/MPI_Alltoall.html

#include <mpi3/communicator.hpp>
#include <mpi3/main.hpp>

#if !defined(EXAMPI)
#include <mpi3/ostream.hpp>
#endif

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

namespace mpi3 = boost::mpi3;

int mpi3::main(int /*argc*/, char** /*argv*/, mpi3::communicator world) try {
	std::size_t const chunk = 5;

	auto sb = std::vector<int>(static_cast<std::size_t>(world.size()) * chunk);
	std::iota(sb.begin(), sb.end(), 40000 + world.rank() * 100);

	auto rb = std::vector<int>(static_cast<std::size_t>(world.size()) * chunk);

	auto sz = static_cast<std::size_t>(world.size());
	assert( sz != 0 );
	assert( sb.size() % sz == 0);

	world.all_to_all_n(sb.data(), sb.size() / sz, rb.data());

#if !defined(EXAMPI)
	mpi3::ostream wout(world);
	std::copy(sb.begin(), sb.end(), std::ostream_iterator<int>(wout << "sb = ", ", "));
	wout << '\n'
	     << std::flush;
	std::copy(rb.begin(), rb.end(), std::ostream_iterator<int>(wout << "rb = ", ", "));
	wout << '\n'
	     << std::flush;

	world.all_to_all_inplace_n(sb.data(), sb.size() / sz);
	// do_all_to_all_n(world, sb.data(), sb.size(), sb.data());
	// world.all_to_all_n(sb.data(), sb.size()); //  , sb.data());
	// std::copy(sb.begin(), sb.end(), std::ostream_iterator<int>(wout<<"sb (inplace) = ", ", ")); wout<<std::endl;
	assert(sb == rb);
#endif

	return 0;
} catch(...) {
	return 0;
}
